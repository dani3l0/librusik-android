function init() {
    document.getElementById("link").addEventListener("click", function() {
        android.openBrowser();
    });
	var buttons = document.getElementsByTagName("button");
	for (var i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener("click", function() {
			this.disabled = true;
			var that = this;
			setTimeout(function() {
				that.disabled = false;
			}, 1000);
		});
	}
    document.body.style.opacity = 1;
}
function reload() {
	setTimeout(function() {
   		document.body.style.opacity = 0;
    	setTimeout(function() {
	    	window.location.href = android.getUrl();
	    }, 450);
	 }, 500);
}