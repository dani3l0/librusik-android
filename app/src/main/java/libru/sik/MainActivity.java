package libru.sik;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.CookieManager;
import android.widget.Toast;
import androidx.core.content.res.ResourcesCompat;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {
    private WebView Librusik;
    View page;
    Intent browser;
    long back_pressed;

    public ValueCallback<Uri[]> uploadMessage;

    CookieManager cookieManager;
    public class nativeBridge {
        nativeBridge() {}
        @JavascriptInterface
        public void syncCookies() {
            cookieManager.flush();
        }
        @JavascriptInterface
        public void openBrowser() {
            startActivity(browser);
        }
        @JavascriptInterface
        public String getUrl() { return getString(R.string.librusik_url); }
    }
    @SuppressLint({"StaticFieldLeak", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        page = this.findViewById(R.id.webView);
        Librusik = findViewById(R.id.webView);
        Librusik.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.background, null));
        Librusik.getSettings().setJavaScriptEnabled(true);
        Librusik.getSettings().setSupportZoom(false);
        Librusik.getSettings().setDomStorageEnabled(false);
        Librusik.getSettings().setGeolocationEnabled(false);
        Librusik.getSettings().setAllowContentAccess(false);
        Librusik.getSettings().setAllowFileAccess(true);
        Librusik.getSettings().setDatabaseEnabled(false);
        Librusik.setHorizontalScrollBarEnabled(false);
        Librusik.addJavascriptInterface(new nativeBridge(), "android");
        Librusik.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.addRequestHeader("Cookie", CookieManager.getInstance().getCookie(getString(R.string.librusik_url)));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);
        });
        cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        browser = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.librusik_url)));
        if (savedInstanceState == null) startWebview(getString(R.string.librusik_url));
    }
    public void startWebview(String uri) {
        Librusik.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String url) {
                Librusik.loadUrl("file:///android_res/raw/error.html");
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                page.setAlpha(1);
                Resources res = getResources();
                InputStream in_s = res.openRawResource(R.raw.librusik);
                try {
                    byte[] b = new byte[in_s.available()];
                    Librusik.evaluateJavascript(new String(b), null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri uri = request.getUrl();
                if (!uri.toString().contains(getString(R.string.librusik_url))) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                else Librusik.loadUrl(uri.toString());
                return true;
            }
        });
        Librusik.setWebChromeClient(new WebChromeClient() {
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                }
                uploadMessage = filePathCallback;
                Intent intent = fileChooserParams.createIntent();
                try {
                    startActivityForResult(intent, 100);
                }
                catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    return false;
                }
                return true;
            }
        });
        Librusik.loadUrl(uri);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 100) {
            if (uploadMessage == null)
                return;
            uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
            uploadMessage = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 1000 > System.currentTimeMillis()) super.onBackPressed();
        else Toast.makeText(getBaseContext(), getString(R.string.exit_confirmation), Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Librusik.saveState(outState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Librusik.restoreState(savedInstanceState);
    }
}
