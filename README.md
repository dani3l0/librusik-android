# Librusik Android

Simple Librusik WebView wrapper for Android.


### Why?

Using Librusik from browser may be annoying. Here's also the browser, but without the address bar.


### Build

Import project in Android Studio and edit instance URL in `strings.xml` file. Then, build it and put the .apk file to be downloadable from Librusik login screen.
